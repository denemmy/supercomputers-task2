Основной код лежит в папке source

source/Puasson_MPI.c - реализация на MPI
source/Puasson_MPI_omp.c - реализация на MPI + OpenMP

Сборка:

source/make_mpi.sh - сборка MPI-версии на BlueGene
source/make_mpi_omp.sh - сборка MPI+OpenMP-версии на BlueGene
source/make_mpi_lom.sh - сборка MPI-версии на Ломоносове

Запуск:

примеры запусков на BlueGene и Ломоносове приведены в файлах:
run_mpi.sh
run_mpi_lom.sh

Построение графиков:
Matlab-код для построения графиков лежит в папке matlab, файлы данных для построения должны лежать в той же папке.