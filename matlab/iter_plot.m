function iter_plot()

    file_path = 'IterNorm_1000x1000_1.log';
    [iters, err_vals] = parse_file(file_path);
    
    figure;
    plot(iters, err_vals);
    axis tight
    ylim([0, 1.2 * max(err_vals(:))]);
    xlabel('iteration');
    ylabel('error');
    grid on;
end


function [iters, err_vals] = parse_file(file_path)

    fp = fopen(file_path, 'r');    
    C = textscan(fp, '%f %f', 'Delimiter', ' ', 'CommentStyle', '#');    
    fclose(fp);
    iters = C{1};
    err_vals = C{2};
end
