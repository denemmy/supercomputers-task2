function plotter()

    file_path = 'PuassonSerial_ECGM_1000x1000_128.dat';
    [X, Y, Z] = parse_file(file_path);
    
    npoints = length(X);
    axis_dims = sqrt(npoints);
    if axis_dims ~= fix(axis_dims)
        printf('Wrong number of points in the input file, should be square.');
    end
    
    X = reshape(X, [axis_dims, axis_dims])';
    Y = reshape(Y, [axis_dims, axis_dims])';
    Z = reshape(Z, [axis_dims, axis_dims])';
    
    N_pt = 100;
    xstep = (max(X(:)) - min(X(:))) / (N_pt - 1);
    xaxis = min(X(:)) + xstep .* (0:N_pt-1);
    
    ystep = (max(Y(:)) - min(Y(:))) / (N_pt - 1);
    yaxis = min(Y(:)) + ystep .* (0:N_pt-1);
    
    [Xi, Yi] = meshgrid(xaxis, yaxis);
    Zi = interp2(X, Y, Z, Xi, Yi);
    
    Zr = real_function(X, Y);
    
    AbsErr = abs(Zr - Z);
    
    coeff = 1 ./ Z;
    coeff(Z < 1e-9) = 0;
    RelErr = AbsErr .* coeff;
    
    figure;
    surf(X, Y, Z, 'EdgeColor','none');
    title('computed function');
    zlim([0, 2.1]);
    camorbit(250, 15);
    xlabel('X');
    ylabel('Y');
    
    figure;
    surf(X, Y, Zr, 'EdgeColor','none');
    title('1 + sin(xy)');
    zlim([0, 2.1]);
    camorbit(250, 15);
    xlabel('X');
    ylabel('Y');    
    
    figure;
    surf(X, Y, AbsErr, 'EdgeColor','none');
    title('absolute error');
    % zlim([min(Zdiff(:)), max(Zdiff(:))]);    
    zlim([0, 1.2 * max(AbsErr(:))]);
    camorbit(250, 15);
    xlabel('X');
    ylabel('Y');
    
    figure;
    surf(X, Y, RelErr, 'EdgeColor','none');
    title('relative error');
    % zlim([min(Zdiff(:)), max(Zdiff(:))]);    
    zlim([0, 1.2 * max(RelErr(:))]);    
    camorbit(250, 15);
    xlabel('X');
    ylabel('Y');
end

function Z = real_function(X, Y)

    Z = 1 + sin(X.*Y);

end


function [X, Y, Z] = parse_file(file_path)

    fp = fopen(file_path, 'r');    
    C = textscan(fp, '%f %f %f', 'Delimiter', ' ', 'CommentStyle', '#');    
    fclose(fp);
    X = C{1};
    Y = C{2};
    Z = C{3};
end
