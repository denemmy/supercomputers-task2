// This is the explicit conjugate gradient method for descrete Puasson problem
// on uniform mesh.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <omp.h>

#define Test 0
// #define Print
#define Step 10
#define PrintStep 1
#define NotOutputRes

#define MIN_ERR 1e-4

#define TRUE  ((int) 1)
#define FALSE ((int) 0)

#define TAG_RES     40
#define TAG_BAS     41
#define TAG_SOLV    42

#define Max(A,B) ((A)>(B)?(A):(B))
#define Min(A,B) ((A)<(B)?(A):(B))
#define R2(x,y) ((x)*(x)+(y)*(y))
#define Cube(x) ((x)*(x)*(x))
#define x(i,h0) ((i)*h0)
#define y(j,h1) ((j)*h1)

#define LeftPart(P,i,j,N0,h0,h1)\
    ((-(P[N0*(j) +i + 1] - P[N0*(j) +i]) / h0 + (P[N0*(j) +i] - P[N0*(j) +i - 1]) / h0) / h0 + \
    (-(P[N0*(j + 1) + i] - P[N0*(j) +i]) / h1 + (P[N0*(j) +i] - P[N0*(j - 1) + i]) / h1) / h1)

double BoundaryValue(double x, double y)
{
    return 1 + sin(x*y);
}

double Solution(double x, double y)
{
    return 1 + sin(x*y);
}

int RightPart(double * rhs, double h0, double h1, int N0, int N1)
{
    int i, j;
    double x, y;

#pragma omp parallel for private(j, i, x, y)
    for (j = 0; j<N1; j++) {
        y = y(j, h1);
        for (i = 0; i<N0; i++) {
            x = x(i, h0);
            rhs[j*N0 + i] = R2(x, y) * sin(x*y);
        }
    }

    return 0;
}

int IsPower(int Number)
// the function returns log_{2}(Number) if it is integer. If not it returns (-1).
{
    unsigned int M;
    int p;

    if (Number <= 0)
        return(-1);

    M = Number; p = 0;
    while (M % 2 == 0)
    {
        ++p;
        M = M >> 1;
    }
    if ((M >> 1) != 0)
        return(-1);
    else
        return(p);

}

int SplitFunction(int N0, int N1, int p)
// This is the splitting procedure of proc. number p. The integer p0
// is calculated such that abs(N0/p0 - N1/(p-p0)) --> min.
{
    float n0, n1;
    int p0, i;

    n0 = (float)N0; n1 = (float)N1;
    p0 = 0;

    for (i = 0; i < p; i++)
        if (n0 > n1)
        {
            n0 = n0 / 2.0;
            ++p0;
        }
        else
            n1 = n1 / 2.0;

    return(p0);
}

void Communicate(MPI_Comm Grid_Comm,
    double *SolVect, int TAG_COMM,
    int N0, double * recvBuff0,
    double *sendBuff1, double *recvBuff1,
    int st0, int len0,
    int st1, int len1,
    int up, int down, int left, int right)
{

    MPI_Status status;
    int i;

    /* ---------------------------------------------- */
    /* ----- send to top and receive from bottom ---- */
    /* ---------------------------------------------- */
    if (up >= 0 && down >= 0) {
        // send first line
        MPI_Sendrecv(&SolVect[N0*st1 + st0], len0, MPI_DOUBLE, up, TAG_COMM,
            recvBuff0, len0, MPI_DOUBLE, down, TAG_COMM, Grid_Comm, &status);
    }
    else if (up >= 0) {
        MPI_Send(&SolVect[N0*st1 + st0], len0, MPI_DOUBLE, up, TAG_COMM, Grid_Comm);
    }
    else if (down >= 0) {
        MPI_Recv(recvBuff0, len0, MPI_DOUBLE, down, TAG_COMM, Grid_Comm, &status);
    }
    if (down >= 0) {
        // update data (line after bottom)
#pragma omp parallel for
        for (i = 0; i < len0; i++)
        {
            SolVect[N0*(st1 + len1) + st0 + i] = recvBuff0[i];
        }
    }

    /* ---------------------------------------------- */
    /* ----- send to bottom and receive from top ---- */
    /* ---------------------------------------------- */
    if (up >= 0 && down >= 0) {
        // send last line
        MPI_Sendrecv(&SolVect[N0*(st1 + len1 - 1) + st0], len0, MPI_DOUBLE, down, TAG_COMM,
            recvBuff0, len0, MPI_DOUBLE, up, TAG_COMM, Grid_Comm, &status);
    }
    else if (down >= 0) {
        MPI_Send(&SolVect[N0*(st1 + len1 - 1) + st0], len0, MPI_DOUBLE, down, TAG_COMM, Grid_Comm);
    }
    else if (up >= 0) {
        MPI_Recv(recvBuff0, len0, MPI_DOUBLE, up, TAG_COMM, Grid_Comm, &status);
    }
    if (up >= 0) {
        // update data (line before top)
#pragma omp parallel for
        for (i = 0; i < len0; i++) {
            SolVect[N0*(st1 - 1) + st0 + i] = recvBuff0[i];
        }
    }

    /* ---------------------------------------------- */
    /* ----- send to right and receive from left ---- */
    /* ---------------------------------------------- */
    // prepare data for sending
    if (right >= 0) {
        // right (last) column
#pragma omp parallel for
        for (i = 0; i < len1; i++) {
            sendBuff1[i] = SolVect[N0*(st1 + i) + st0 + len0 - 1];
        }
    }
    if (left >= 0 && right >= 0) {
        MPI_Sendrecv(sendBuff1, len1, MPI_DOUBLE, right, TAG_COMM,
            recvBuff1, len1, MPI_DOUBLE, left, TAG_COMM, Grid_Comm, &status);
    }
    else if (right >= 0) {
        MPI_Send(sendBuff1, len1, MPI_DOUBLE, right, TAG_COMM, Grid_Comm);
    }
    else if (left >= 0) {
        MPI_Recv(recvBuff1, len1, MPI_DOUBLE, left, TAG_COMM, Grid_Comm, &status);
    }
    if (left >= 0) {
        // update data (column before left)
#pragma omp parallel for
        for (i = 0; i < len1; i++) {
            SolVect[N0*(st1 + i) + st0 - 1] = recvBuff1[i];
        }
    }


    /* ---------------------------------------------- */
    /* ----- send to left and receive from right ---- */
    /* ---------------------------------------------- */
    // prepare data for sending
    if (left >= 0) {
        // left (first) column
#pragma omp parallel for
        for (i = 0; i < len1; i++)
        {
            sendBuff1[i] = SolVect[N0*(st1 + i) + st0];
        }
    }
    if (left >= 0 && right >= 0) {
        MPI_Sendrecv(sendBuff1, len1, MPI_DOUBLE, left, TAG_COMM,
            recvBuff1, len1, MPI_DOUBLE, right, TAG_COMM, Grid_Comm, &status);
    }
    else if (left >= 0) {
        MPI_Send(sendBuff1, len1, MPI_DOUBLE, left, TAG_COMM, Grid_Comm);
    }
    else if (right >= 0) {
        MPI_Recv(recvBuff1, len1, MPI_DOUBLE, right, TAG_COMM, Grid_Comm, &status);
    }
    if (right >= 0) {
        // update data (column after right)
#pragma omp parallel for
        for (i = 0; i < len1; i++)
        {
            SolVect[N0*(st1 + i) + st0 + len0] = recvBuff1[i];
        }
    }

    return;
}

int main(int argc, char * argv[])
{

    // Domain size.
    const double A = 2.0;
    const double B = 2.0;

    int N0, N1;                             // the number of internal points on axes (ox) and (oy).
    double h0, h1;                          // mesh steps on (0x) and (0y) axes
    int n0, n1, k0, k1;                       // N0 = n0*dims[0] + k0, N1 = n1*dims[1] + k1.
    int len0, len1;                         // subregion size
    int st0, st1;                           // subregion start index
    int en0, en1;                           // subregion end index (not including)

    int ProcNum, rank;                      // the number of processes and rank in communicator.
    int power, p0, p1;                      // ProcNum = 2^(power), power splits into sum p0 + p1.
    int dims[2];                            // dims[0] = 2^p0, dims[1] = 2^p1 (--> M = dims[0]*dims[1]).

    int Coords[2];                          // the process coordinates in the cartesian topology created for mesh.
    MPI_Comm Grid_Comm;                     // this is a handler of a new communicator.
    const int ndims = 2;                    // the number of a process topology dimensions.
    int periods[2] = { 0, 0 };                 // it is used for creating processes topology.
    int left, right, up, down;              // the neighbours of the process.

    double *SolVect;                        // the solution array
    double *ResVect;                        // the residual array
    double *BasisVect;                      // the vector of A-orthogonal system in CGM
    double * RHS_Vect;                      // the right hand side of Puasson equation.
    double sp, alpha, tau, NewValue, err;   // auxiliary values
    double alphaLocal, spLocal, tauLocal;
    double errLocal;
    double curError;
    double compErr;
    int SDINum, CGMNum;                     // the number of steep descent and CGM iterations.
    int counterSDI;                         // the current iteration number.
    int counterCGM;                         // the current iteration number.

    double *recvBuff0;                      // buffer for receiving data on axis 0
    double *sendBuff1;                      // same for axis 1
    double *recvBuff1;                      // for axis 1

    int i, j;
    char str[127];
    FILE * fp;

    double startTime, endTime, totalTime;

    // MPI Library is being activated ...
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // command line analizer
    switch (argc)
    {
    case 4: {
        SDINum = 1;
        CGMNum = atoi(argv[3]);
        break;
    }
    case 5: {
        SDINum = Max(atoi(argv[3]), 1);      // SDINum >= 1
        CGMNum = atoi(argv[4]);
        break;
    }
    default: {
        if (rank == 0)
            printf("Wrong number of parameters in command line.\nUsage: <ProgName> "
                "<Nodes number on (0x) axis> <Nodes number on (0y) axis> "
                "[the number of steep descent iterations] "
                "<the number of conjugate gragient iterations>\nFinishing...\n");
        MPI_Finalize();
        return(1);
    }
    }

    N0 = atoi(argv[1]); N1 = atoi(argv[2]);
    h0 = A / (N0 - 1);    h1 = B / (N1 - 1);

    if ((N0 <= 0) || (N1 <= 0))
    {
        if (rank == 0)
            printf("The first and the second arguments (mesh numbers) should be positive.\n");

        MPI_Finalize();
        return(2);
    }

    if ((power = IsPower(ProcNum)) < 0)
    {
        if (rank == 0)
            printf("The number of procs must be a power of 2.\n");

        MPI_Finalize();
        return(3);
    }

    p0 = SplitFunction(N0, N1, power);
    p1 = power - p0;

    dims[0] = (unsigned int)1 << p0;   dims[1] = (unsigned int)1 << p1;
    n0 = N0 >> p0;                      n1 = N1 >> p1;
    k0 = N0 - dims[0] * n0;             k1 = N1 - dims[1] * n1;

    // the cartesian topology of processes is being created ...
    MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, TRUE, &Grid_Comm);
    MPI_Comm_rank(Grid_Comm, &rank);
    MPI_Cart_coords(Grid_Comm, rank, ndims, Coords);

    MPI_Cart_shift(Grid_Comm, 0, 1, &left, &right);
    MPI_Cart_shift(Grid_Comm, 1, -1, &down, &up);

    // start coord
    st0 = Min(k0, Coords[0]) * (n0 + 1) + Max(0, (Coords[0] - k0)) * n0;
    st1 = Min(k1, Coords[1]) * (n1 + 1) + Max(0, (Coords[1] - k1)) * n1;

    // not including end coord (0...en0-1)
    en0 = st0 + n0 + (Coords[0] < k0 ? 1 : 0);
    en1 = st1 + n1 + (Coords[1] < k1 ? 1 : 0);

    // do not include boundary
    st0 = Max(1, st0);
    st1 = Max(1, st1);

    en0 = Min(N0 - 1, en0);
    en1 = Min(N1 - 1, en1);

    // length
    len0 = en0 - st0;
    len1 = en1 - st1;

    totalTime = 0;

    if (rank == 0)
    {
        sprintf(str, "PuassonSerial_ECGM_%dx%d_%d.log", N0, N1, ProcNum);
        fp = fopen(str, "w");
        fprintf(fp, "The Domain: [0,%f]x[0,%f], number of points: N[0,A] = %d, N[0,B] = %d;\n", A, B, N0, N1);
        printf("Number of precesses: %d\n", ProcNum);
        printf("The Domain: [0,%f]x[0,%f], number of points: N[0,A] = %d, N[0,B] = %d;\n", A, B, N0, N1);
    }

    SolVect = (double *)malloc(N0*N1 * sizeof(double));
    ResVect = (double *)malloc(N0*N1 * sizeof(double));
    RHS_Vect = (double *)malloc(N0*N1 * sizeof(double));

    recvBuff0 = (double *)malloc((n0 + 1) * sizeof(double));
    sendBuff1 = (double *)malloc((n1 + 1) * sizeof(double));
    recvBuff1 = (double *)malloc((n1 + 1) * sizeof(double));

    if (rank == 0)
    {
        printf("Computing boundary values...\n");
    }

    // Initialization of Arrays
    memset(ResVect, 0, N0*N1 * sizeof(double));
    RightPart(RHS_Vect, h0, h1, N0, N1);

    memset(SolVect, 0, N0*N1 * sizeof(double));
#pragma omp parallel for private(i)
    for (i = 0; i<N0; i++)
    {
        SolVect[i] = BoundaryValue(x(i, h0), 0.0);
        SolVect[N0*(N1 - 1) + i] = BoundaryValue(x(i, h0), B);
    }
#pragma omp parallel for private(j)
    for (j = 0; j<N1; j++)
    {
        SolVect[N0*j] = BoundaryValue(0.0, y(j, h1));
        SolVect[N0*j + (N0 - 1)] = BoundaryValue(A, y(j, h1));
    }

    if (rank == 0)
    {
        printf("Boundary values computed.\n");
        printf("Running SDI iterations...\n");
    }

    startTime = MPI_Wtime();
    for (counterSDI = 1; counterSDI <= SDINum; counterSDI++)
    {
        // The residual vector r(k) = Ax(k)-f is calculating ...
#pragma omp parallel for private(j, i)
        for (j = st1; j < en1; j++)
        {
            for (i = st0; i < en0; i++)
            {
                ResVect[N0*j + i] = LeftPart(SolVect, i, j, N0, h0, h1) - RHS_Vect[N0*j + i];
            }
        }

        // The value of product (r(k),r(k)) is calculating ...
        spLocal = 0.0;
#pragma omp parallel for private(j, i) reduction(+:spLocal)
        for (j = st1; j < en1; j++)
        {
            for (i = st0; i < en0; i++)
            {
                spLocal += ResVect[N0*j + i] * ResVect[N0*j + i] * h0*h1;
            }
        }

        // Reduce some for all processes
        MPI_Allreduce(&spLocal, &sp, 1, MPI_DOUBLE, MPI_SUM, Grid_Comm);
        tau = sp;

        // Update ResVect region borders as it is needed for computing Ar(k)
        Communicate(Grid_Comm, ResVect, TAG_RES, N0, recvBuff0, sendBuff1, recvBuff1, st0, len0, st1, len1, up, down, left, right);

        // The value of product sp = (Ar(k),r(k)) is calculating ...
        spLocal = 0.0;
#pragma omp parallel for private(j, i) reduction(+:spLocal)
        for (j = st1; j < en1; j++)
        {
            for (i = st0; i < en0; i++)
            {
                spLocal += LeftPart(ResVect, i, j, N0, h0, h1)*ResVect[N0*j + i] * h0*h1;
            }
        }

        // Reduce some for all processes
        MPI_Allreduce(&spLocal, &sp, 1, MPI_DOUBLE, MPI_SUM, Grid_Comm);
        tau = tau / sp;

        // The x(k+1) is calculating ...
        errLocal = 0.0;
#pragma omp parallel for private(j, i, NewValue, curError)
        for (j = st1; j < en1; j++)
        {
            for (i = st0; i < en0; i++)
            {
                NewValue = SolVect[N0*j + i] - tau*ResVect[N0*j + i];
                curError = fabs(NewValue - SolVect[N0*j + i]);
                SolVect[N0*j + i] = NewValue;

                if (curError > errLocal) {
#pragma omp critical
                    {
                        if (curError > errLocal)
                        {
                            errLocal = curError;
                        }
                    }
                }
            }
        }

        MPI_Allreduce(&errLocal, &err, 1, MPI_DOUBLE, MPI_MAX, Grid_Comm);

        // Update solution on region borders
        Communicate(Grid_Comm, SolVect, TAG_SOLV, N0, recvBuff0, sendBuff1, recvBuff1, st0, len0, st1, len1, up, down, left, right);

#ifdef Print
        if (rank == 0 && (counterSDI % PrintStep == 0))
        {
            printf("SDI iteration %d, error=%f\n", counterSDI, err);
        }
#endif
    }
    counterSDI--;
    endTime = MPI_Wtime();
    totalTime += (endTime - startTime);
    // the end of steep descent iteration.

    if (rank == 0)
    {
        printf("SDI iterations finished, number of SDI iterations = %d, last error = %f\n", counterSDI, err);
        printf("Running CGM iterations...\n");
    }

    BasisVect = ResVect;    // g(0) = r(k-1).
    ResVect = (double *)malloc(N0*N1 * sizeof(double));
    memset(ResVect, 0, N0*N1 * sizeof(double));

    // CGM iterations begin ...
    // sp == (Ar(k-1),r(k-1)) == (Ag(0),g(0)), k=1.
    startTime = MPI_Wtime();
    for (counterCGM = 1; counterCGM <= CGMNum; counterCGM++)
    {
        // The residual vector r(k) is calculating ...
#pragma omp parallel for private(j, i)
        for (j = st1; j < en1; j++)
        {
            for (i = st0; i < en0; i++)
            {
                ResVect[N0*j + i] = LeftPart(SolVect, i, j, N0, h0, h1) - RHS_Vect[N0*j + i];
            }
        }

        // Update ResVect region borders as it is needed for computing Ar(k)
        Communicate(Grid_Comm, ResVect, TAG_RES, N0, recvBuff0, sendBuff1, recvBuff1, st0, len0, st1, len1, up, down, left, right);

        // The value of product (Ar(k),g(k-1)) is calculating ...
        alphaLocal = 0.0;
#pragma omp parallel for private(j, i) reduction(+:alphaLocal)
        for (j = st1; j < en1; j++)
        {
            for (i = st0; i < en0; i++)
            {
                alphaLocal += LeftPart(ResVect, i, j, N0, h0, h1)*BasisVect[N0*j + i] * h0*h1;
            }
        }

        // Reduce some for all processes
        MPI_Allreduce(&alphaLocal, &alpha, 1, MPI_DOUBLE, MPI_SUM, Grid_Comm);
        alpha = alpha / sp;

        // The new basis vector g(k) is being calculated ...
#pragma omp parallel for private(j, i)
        for (j = st1; j < en1; j++)
        {
            for (i = st0; i < en0; i++)
            {
                BasisVect[N0*j + i] = ResVect[N0*j + i] - alpha*BasisVect[N0*j + i];
            }
        }


        // The value of product (r(k),g(k)) is being calculated ...
        tauLocal = 0.0;
#pragma omp parallel for private(j, i) reduction(+:tauLocal)
        for (j = st1; j < en1; j++)
        {
            for (i = st0; i < en0; i++)
            {
                tauLocal += ResVect[N0*j + i] * BasisVect[N0*j + i] * h0*h1;
            }
        }

        // Reduce some for all processes
        MPI_Allreduce(&tauLocal, &tau, 1, MPI_DOUBLE, MPI_SUM, Grid_Comm);

        // Update BasisVect region borders as it is needed for computing Ag(k)
        Communicate(Grid_Comm, BasisVect, TAG_BAS, N0, recvBuff0, sendBuff1, recvBuff1, st0, len0, st1, len1, up, down, left, right);

        // The value of product sp = (Ag(k),g(k)) is being calculated ...
        spLocal = 0.0;
#pragma omp parallel for private(j, i) reduction(+:spLocal)
        for (j = st1; j < en1; j++)
        {
            for (i = st0; i < en0; i++)
            {
                spLocal += LeftPart(BasisVect, i, j, N0, h0, h1)*BasisVect[N0*j + i] * h0*h1;
            }
        }

        // Reduce sum for all processes
        MPI_Allreduce(&spLocal, &sp, 1, MPI_DOUBLE, MPI_SUM, Grid_Comm);
        tau = tau / sp;

        // The x(k+1) is being calculated ...
        errLocal = 0.0;
#pragma omp parallel for private(j, i, NewValue, curError)
        for (j = st1; j < en1; j++)
        {
            for (i = st0; i < en0; i++)
            {
                NewValue = SolVect[N0*j + i] - tau*BasisVect[N0*j + i];
                curError = fabs(NewValue - SolVect[N0*j + i]);
                SolVect[N0*j + i] = NewValue;
                if (curError > errLocal) {
#pragma omp critical
                    {
                        if (curError > errLocal)
                        {
                            errLocal = curError;
                        }
                    }
                }
            }
        }

        MPI_Allreduce(&errLocal, &err, 1, MPI_DOUBLE, MPI_MAX, Grid_Comm);

        Communicate(Grid_Comm, SolVect, TAG_SOLV, N0, recvBuff0, sendBuff1, recvBuff1, st0, len0, st1, len1, up, down, left, right);

#ifdef Print
        if (rank == 0 && (counterCGM % PrintStep == 0))
        {
            printf("CGM iteration %d, error=%f\n", counterCGM, err);
        }
#endif

        if (err < MIN_ERR)
        {
            break;
        }
    }
    counterCGM--;
    endTime = MPI_Wtime();
    totalTime += (endTime - startTime);
    // the end of CGM iterations.

    if (rank == 0)
    {
        printf("CGM iterations finished, number of CGM iterations = %d, last error = %f\n", counterCGM, err);
        printf("Gathering results...\n");
    }

    // Gather results
    startTime = MPI_Wtime();
    int sendCount = 4 + (n0 + 1)*(n1 + 1); // <st0><en0><st1><en1><actual data>[<empty>]
    double *sendData = (double *)malloc(sendCount * sizeof(double));
    sendData[0] = (double)(st0);
    sendData[1] = (double)(en0);
    sendData[2] = (double)(st1);
    sendData[3] = (double)(en1);
    for (j = 0; j < en1 - st1; j++)
    {
        for (i = 0; i < en0 - st0; i++)
        {
            sendData[4 + (n0 + 1)*j + i] = SolVect[N0*(j + st1) + i + st0];
        }
    }

    int recvCount = sendCount;
    double *recvData = NULL;
    if (rank == 0)
    {
        recvData = (double *)malloc(ProcNum*recvCount * sizeof(double));
    }
    MPI_Gather(sendData, sendCount, MPI_DOUBLE, recvData, recvCount, MPI_DOUBLE, 0, Grid_Comm);

    endTime = MPI_Wtime();
    totalTime += (endTime - startTime);

    if (rank == 0)
    {
        double *curData = recvData;
        for (int k = 0; k < ProcNum; k++)
        {
            st0 = (int)round(curData[0]);
            en0 = (int)round(curData[1]);
            st1 = (int)round(curData[2]);
            en1 = (int)round(curData[3]);
            // #pragma omp parallel for private(j,i)
            for (int j = 0; j < en1 - st1; j++)
            {
                for (int i = 0; i < en0 - st0; i++)
                {
                    SolVect[N0*(j + st1) + i + st0] = curData[4 + (n0 + 1)*j + i];
                }
            }

            curData += recvCount;
        }

        printf("Results gathered.\n");
        printf("Total time: %f sec\n", totalTime);
        printf("Computing error...\n");

        // compare with the solution
        double maxErr = 0.0;
        double x;
        double y;
        double gtVal;
        double compCalc;
        for (int j = 0; j < N1; j++)
        {
            y = y(j, h1);
            for (int i = 0; i < N0; i++)
            {
                x = x(i, h0);
                compCalc = SolVect[N0 * j + i];
                gtVal = Solution(x, y);
                maxErr = Max(maxErr, (compCalc - gtVal));
            }
        }
        compErr = maxErr;
        printf("Real error = %f\n", compErr);

        printf("Writing results...\n");

        // printing some results ...
        fprintf(fp, "The steep descent iterations number: %d\nThe conjugate gradient iterations number: %d\n", counterSDI, counterCGM);
        fprintf(fp, "The %d iterations are carried out. Time for computing is %f sec. The real error is %.12f.\n",
            counterSDI + counterCGM, totalTime, compErr);
        fclose(fp);

#if !defined(NotOutputRes)
        sprintf(str, "PuassonSerial_ECGM_%dx%d_%d.dat", N0, N1, ProcNum);
        fp = fopen(str, "w");
        fprintf(fp, "# This is the conjugate gradient method for descrete Puasson equation.\n"
            "# A = %f, B = %f, N[0,A] = %d, N[0,B] = %d, SDINum = %d, CGMNum = %d.\n"
            "# One can draw it by gnuplot by the command: splot 'MyPath\\FileName.dat' with lines\n", \
            A, B, N0, N1, counterSDI, counterCGM);
        for (j = 0; j < N1; j++)
        {
            for (i = 0; i < N0; i++)
                fprintf(fp, "\n%f %f %f", x(i, h0), y(j, h1), SolVect[N0*j + i]);
            fprintf(fp, "\n");
        }
        fclose(fp);
#endif
    }

    if (rank == 0)
    {
        printf("Finihsed writing.\n");
        printf("Finalize program...\n");
    }

    free(sendData);
    if (recvData != NULL)
    {
        free(recvData);
    }

    free(recvBuff0); free(sendBuff1); free(recvBuff1);
    free(SolVect); free(ResVect); free(BasisVect); free(RHS_Vect);

    MPI_Finalize();

    if (rank == 0)
    {
        printf("All done.\n");
    }

    return(0);
}
