#!/bin/bash -x
sbatch -n128 -p test -t 0-00:15:00 impi ./Puasson_MPI 1000 1000 1 10000
sbatch -n32 -p test -t 0-00:15:00 impi ./Puasson_MPI 1000 1000 1 10000
sbatch -n16 -p test -t 0-00:15:00 impi ./Puasson_MPI 1000 1000 1 10000
sbatch -n8 -p test -t 0-00:15:00 impi ./Puasson_MPI 1000 1000 1 10000
sbatch -n1 -p test -t 0-00:15:00 impi ./Puasson_MPI 1000 1000 1 10000

sbatch -n128 -p test -t 0-00:15:00 impi ./Puasson_MPI 2000 2000 1 10000
sbatch -n32 -p test -t 0-00:15:00 impi ./Puasson_MPI 2000 2000 1 10000
sbatch -n16 -p test -t 0-00:15:00 impi ./Puasson_MPI 2000 2000 1 10000
sbatch -n8 -p test -t 0-00:15:00 impi ./Puasson_MPI 2000 2000 1 10000
sbatch -n1 -p test -t 0-00:15:00 impi ./Puasson_MPI 2000 2000 1 10000