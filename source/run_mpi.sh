#!/bin/bash -x
mpisubmit.bg -n 1 -w 00:15:00 -m smp -e "OMP_NUM_THREADS=3" Puasson_MPI 1000 1000 1 10000
mpisubmit.bg -n 1 -w 01:30:00 -m smp -e "OMP_NUM_THREADS=3" Puasson_MPI 2000 2000 1 10000

mpisubmit.bg -n 256 -w 00:05:00 -m smp -e "OMP_NUM_THREADS=3" Puasson_MPI 1000 1000 1 10000